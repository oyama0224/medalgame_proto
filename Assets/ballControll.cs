﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class ballControll : NetworkBehaviour {
    SlotControll slot;
    Rigidbody rigidBody;
	// Use this for initialization
	void Start () {
        //slot = GameObject.Find("Slot").GetComponent<SlotControll>();
        rigidBody = GetComponent<Rigidbody>();
	}
    public override void OnStartServer()
    {
        base.OnStartServer();
        slot = GameObject.Find("Slot").GetComponent<SlotControll>();
    }
    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerExit(Collider collider)
    {
        if(collider.tag == "BallEnd")
        {
            rigidBody.position = transform.parent.position;
            rigidBody.velocity = Vector3.zero;
            rigidBody.angularVelocity = Vector3.zero;
            if(isServer)
                slot.StartSlot();
        }

    }
}
