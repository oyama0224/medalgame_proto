﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class ShooterScript : NetworkBehaviour {

    float baseAngle;
    int ID;
   

    static Vector2 mousePos;
    static int nowUse;
    static int shooterCount;
    static GameObject medalPrefab;
    static List<Transform> shooterTransforms;
    static float limitAngle = 75.0f;
    static Vector3 spawnOffset = new Vector3(0, 1, 0);
    static bool changeCoolDown;


	// Use this for initialization
	void Start () {
        baseAngle = transform.eulerAngles.y;
        if(medalPrefab == null)
            medalPrefab = Resources.Load<GameObject>("Prefab/Medal");
        ID = shooterCount;
        if (shooterTransforms == null)
            shooterTransforms = new List<Transform>();
        shooterTransforms.Add(transform);
        shooterCount++;
	}
	
	// Update is called once per frame
	void Update () {
        if(nowUse == ID){
            mousePos = Input.mousePosition;
            shooterTransforms[nowUse].eulerAngles = new Vector3(transform.eulerAngles.x,
                                         baseAngle + (mousePos.x - Screen.width / 2) / Screen.width / 2 * limitAngle,
                                         transform.eulerAngles.z);
            if (Input.GetMouseButtonDown(0))
            {
                GameObject medal = GameObject.Instantiate(medalPrefab);
                if(isServer)    
                    NetworkServer.Spawn(medal);
                else
                {
                   
                }
                medal.transform.position = shooterTransforms[nowUse].position + spawnOffset;
                //medal.transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 90);
                medal.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward) * 80);
                medal.GetComponent<MedalControll>().isSpawn = true;
                MyMedal.medal--;
            }
            if (Input.GetMouseButtonUp(1) && !changeCoolDown)
            {
                changeCoolDown = true;
                nowUse = (nowUse + 1) % shooterCount;
                StartCoroutine(CoolDownCoroutine());
            }
        }
	}
    IEnumerator CoolDownCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        changeCoolDown = false;
        System.GC.Collect();
    }
}
