﻿using UnityEngine;
using System.Collections;

public class CameraControll : MonoBehaviour {

    Vector3 basePos;

    float radius;
    float height = 30.0f;
    Vector2 lookAngle;
    float sensitivity = 2.0f;

    Vector2 inputAxis;
    // Use this for initialization
    void Start () {
        lookAngle.y = 180.0f;
        lookAngle.x = 45.0f;
        basePos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        inputAxis = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            //視点操作
            if(inputAxis.x != 0)
                lookAngle.y -= inputAxis.x * sensitivity;
            if(inputAxis.y != 0)
                lookAngle.x = Mathf.Clamp(lookAngle.x + (inputAxis.y * sensitivity), -20, 90);

            transform.eulerAngles = lookAngle;
            radius = 25.0f - Mathf.Abs(lookAngle.x) / 90.0f * 20.0f;
            transform.position = new Vector3(-Mathf.Sin(lookAngle.y * Mathf.Deg2Rad) * radius, (lookAngle.x / 90) * height, -Mathf.Cos(lookAngle.y * Mathf.Deg2Rad) * radius);



    }
}
