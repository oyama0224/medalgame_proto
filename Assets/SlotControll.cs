﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class SlotControll : NetworkBehaviour
{
    TextMesh[] reelText = new TextMesh[3];
    int[] reelNum = new int[3];
    bool[] reelStop = new bool[3];
    float count;
    bool isUse;
    PayOutScript payout;
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            reelText[i] = transform.GetChild(i).GetComponent<TextMesh>();
            reelNum[i] = Random.Range(0, 9);
            reelText[i].text = reelNum[i].ToString();
        }
       
    }
    public override void OnStartServer()
    {
        base.OnStartServer();
        payout = GameObject.Find("PayOut").GetComponent<PayOutScript>();
    }
    // Update is called once per frame
    [Server]void FixedUpdate()
    {

        if (isUse)
        {
            SlotUpdate();
        }

    }
    [Server]void SlotUpdate()
    {
        count += Time.fixedDeltaTime;


        //回転
        for (int i = (int)count; i < 3; i++)
        {
            reelNum[i] = (reelNum[i] + 1) % 10;
            reelText[i].text = reelNum[i].ToString();
        }

        //決定
        for (int j = 0; j < (int)count; j++)
        {
            if (!reelStop[j])
            {
                if (j != 0 && Random.value * 10 < 5)
                {
                    reelNum[j] = reelNum[j - 1];
                    reelText[j].text = reelNum[j].ToString();
                    reelStop[j] = true;
                }
                else
                {

                    
                    reelNum[j] += Random.Range(0, 9);
                    if(j != 0)
                    {
                        if(reelNum[j] == reelNum[j - 1])
                        {
                            reelNum[j] += Random.Range(1, 9);
                        }
                    }
                    reelNum[j] %= 10;

                    reelText[j].text = reelNum[j].ToString();

                    reelStop[j] = true;
                }
            }
        }

        if (count > 3.5f)
        {
            if (reelNum[0] == reelNum[1] && reelNum[1] == reelNum[2])
            {
                payout.Pay(100);
            }
            isUse = false;
        }





    }
    [Server]public void StartSlot()
    {
        isUse = true;
        payout.Pay(30);
    }
}
