﻿using UnityEngine;
using System.Collections;

public class MedalControll : MonoBehaviour {
    static float fieldSize = 8.0f;
    static GameObject medalGroup;

    static Vector3 spawnAngle = new Vector3(0,0,90);
    static Vector3 spawnoffset = new Vector3(0, 0.4f, 0);

    public bool isSpawn;
    Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
        if(medalGroup == null)
            medalGroup = GameObject.FindGameObjectWithTag("MedalGroup");
        transform.parent = medalGroup.transform;
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        rigidbody.mass = (fieldSize - transform.position.z) * 100;

        if (isSpawn)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, 2.0f, 1 << 8) && isSpawn)
            {
                transform.eulerAngles = hit.transform.eulerAngles + spawnAngle;
                rigidbody.MovePosition(hit.point + spawnoffset);
            }
            else
            {
                isSpawn = false;
            }
        }
        if (transform.position.y < -5)
            Destroy(gameObject);
	}
}
