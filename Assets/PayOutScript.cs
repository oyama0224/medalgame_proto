﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PayOutScript : NetworkBehaviour
{
    bool Online;
    GameObject medalPrefab;
    Transform[] shooter = new Transform[2];
    public int remaining_count;
    public int debug_count = 0;

    float time;
    const float splitTime = 0.1f;
    Vector3 payout_Target = new Vector3(0, 0, -3);
    // Use this for initialization
    void Start()
    {
        medalPrefab = Resources.Load<GameObject>("Prefab/Medal");
        for (int i = 0; i < 2; i++)
        {
            shooter[i] = transform.GetChild(i);
        }
        remaining_count = debug_count;
    }

    // Update is called once per frame
    void FixedUpdate()
    {


        if (!isServer)
            return;

        if (remaining_count > 0)
        {
            time -= Time.fixedDeltaTime;
            if (time <= 0.0f)
            {
                PayOut();
                time += splitTime;
            }
        }else
        {
            time = 0.0f;
        }
    }
    [Server]void PayOut()
    {
        GameObject tempMedal = GameObject.Instantiate<GameObject>(medalPrefab);
        NetworkServer.Spawn(tempMedal);
        tempMedal.transform.position = shooter[remaining_count % 2].position;
        tempMedal.GetComponent<Rigidbody>().AddForce((payout_Target - shooter[remaining_count % 2].position) * 20);
        remaining_count--;
        if(remaining_count <= 0)
        {
            System.GC.Collect();
        }
    }


    public void Pay(int num)
    {
        remaining_count += num;
    }
}
