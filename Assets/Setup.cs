﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Setup : MonoBehaviour {
    Vector3 spawnPos = new Vector3(0, 0, -5);
    GameObject medalPrefab;
    float spawnRadius = 10.0f;
    int turn = 4;
    int turnSpawn = 10;
	// Use this for initialization
	void Start () {
        medalPrefab = Resources.Load<GameObject>("Prefab/Medal");
        //MedalSpawn();


    }
	
	// Update is called once per frame
	void Update () {

	}

    void OnConnectedToServer()
    {
        MedalSpawn();
    }

    void MedalSpawn()
    {
        for (int area = 0; area < 4; area++)
        {
            for (int i = 0; i < turn; i++)
            {
                for (int j = 0; j < turnSpawn; j++)
                {
                    GameObject medalTemp = Instantiate(medalPrefab);
                    medalTemp.transform.position = spawnPos + new Vector3(Mathf.Cos(j / (float)turnSpawn * Mathf.PI * 2) * 3, 1, Mathf.Sin(j / (float)turnSpawn * Mathf.PI * 2) + area * 3);
                }
                System.GC.Collect();
            }

        }
    }
}
