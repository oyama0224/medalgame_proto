﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MyMedal : MonoBehaviour
{
    public static int medal;
    int private_medal;
    public int default_medal = 150;
    Text text;
    // Use this for initialization
    void Start()
    {
        medal = default_medal;
        text = transform.FindChild("Canvas").transform.GetChild(0).GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        text.rectTransform.anchoredPosition = new Vector2(Screen.width / 2, -Screen.height / 2 + text.rectTransform.sizeDelta.y / 2);
        if (medal != private_medal)
        {
            private_medal = medal;
            text.text = private_medal.ToString() + "枚";
        }
    }
}
