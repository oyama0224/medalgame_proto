﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PusherControll : NetworkBehaviour {
    public Vector3 moveSpace = new Vector3(0,0,-2.5f);
    Vector3 basePos;
    float count;
    public float splitTime = 2.0f;
	// Use this for initialization
	void Start () {
        basePos = transform.position - moveSpace;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        GetComponent<Rigidbody>().MovePosition(basePos + (moveSpace * Mathf.Sin(count * Mathf.PI)));
        //transform.position = basePos + (moveSpace * Mathf.Sin(count * Mathf.PI));
        count += Time.fixedDeltaTime / splitTime;
        if (count > 2.0f)
            count -= 2.0f;
	}
}
